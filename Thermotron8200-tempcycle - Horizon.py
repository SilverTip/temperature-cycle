#Thermotron8200Control.py
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-09-09

import pyvisa as visa
import time
import datetime

TChamber_VISA = 'TCPIP0::10.0.10.151::8888::SOCKET'

MINUTE = 60 #unit: second #################### change back to 60 later
HOUR = 60 * 60 #Unit: seconds

def TimeStamp():
    timeS = datetime.datetime.now()
    return timeS, timeS.strftime("%Y-%m-%d %H:%M:%S")

def Init(VISA):
    global TChamber, rm
    rm = visa.ResourceManager()
    TChamber = rm.open_resource(TChamber_VISA)
    TChamber.timeout = 10000

def TChamberReadWrite(inst, cmd): #Manually read byte string becasue visa.read_raw() and visa.query_ascii_values() doesn't work
    character = ''
    string = ''
    inst.write(cmd)
    while(character != '\n'):    
        character = inst.read_bytes(1)
        character = character.decode("utf-8")
        string += character       
    return string

def STOP(): #STOP - stop temperature chamber
    cmd = 'STOP'
    try:
        message = TChamberReadWrite(TChamber, cmd)
    except Exception as e:
        print('Failure to communicate.')
        print(e)
    # print(message)

def TEMP(): #PVAR1? - read the current chamber chanel 1 temperature
    cmd = 'PVAR1?'
    try:
        message = TChamberReadWrite(TChamber, cmd)
    except Exception as e:
        print('Failure to communicate.')
        print(e)
    return message    

def START(): #RUNM - run chamber in manual mode
    cmd = 'RUNM'
    try:
        message = TChamberReadWrite(TChamber, cmd)
    except Exception as e:
        print('Failure to communicate.')
        print(e)
    # print(message)

def HOLD(): #HOLD - hold current temp (even if no setpoint)
    cmd = 'HOLD'
    try:
        message = TChamberReadWrite(TChamber, cmd)
    except Exception as e:
        print('Failure to communicate.')
        print(e)
    # print(message)

def RESUME(): #RESM - Resume manual mode (from hold function)
    cmd = 'RESM'
    try:
        message = TChamberReadWrite(TChamber, cmd)
    except Exception as e:
        print('Failure to communicate.')
        print(e)
    # print(message)

def SET(temp):
    x = 'SETP1'
    cmd = "%s, %s" % (x, temp)
    print(cmd)
    try:
        message = TChamberReadWrite(TChamber, cmd)
    except Exception as e:
        print('Failure to communicate.')
        print(e)
    # print(message)

def RAMP(rrate):
    rrate = rrate
    x = 'MRMP1'
    cmd = "%s, %s" % (x, rrate)
    print(cmd)
    try:
        message = TChamberReadWrite(TChamber, cmd)
    except Exception as e:
        print('Failure to communicate.')
        print(e)
    # print(message)
    

def RunProfile(profile):
    testDuration = 0
    for temp, period in profile:
        testDuration += period
    print('Starting Profile')
    print('Test Duration:(Hours)', round(testDuration,1))
    
    START()
    for temperature, period in profile:     
        current_temp = TEMP()
        ramp_rate = round(abs(float(current_temp) - float(temperature)) / (float(period)*60),2) #Time in hours
        SET(temperature)
        time.sleep(0.25)
        RAMP(ramp_rate)
        print('Time:', TimeStamp()[1], 'Current Temperature:', current_temp, 'Next Temperature:', temperature, 'Ramp:(C/min):', ramp_rate, 'Duration:(Hours)', period) 
        time.sleep(period * HOUR)
      
    print('Test Completed @', TimeStamp()[1])
    STOP()


PROFILE1 = [(25,0.5),
            (25,1),
            (60,0.5),
            (60,1),
            (-40,1),
            (-40,1),
            (25,0.5)]
            
    
#-------Main--------#
Init(TChamber_VISA)
try:
    RunProfile(PROFILE1)
except KeyboardInterrupt:
    print('Keyboard interrupt detected. Stopping script.')
    sys.exit()


#Useful commands: (All channel 1 commands)
#--------------------------
#SETP1,[temperature] - set temperature setpoint
#STOP - stop temperature cycle
#RUNM - run chamber in manual mode
#HOLD - hold current temp (even if no setpoint)
#RESM - Resume manual mode (from hold function)
#MRMP1,[ramp rate] - set manual ranmp rate (degree/min)
